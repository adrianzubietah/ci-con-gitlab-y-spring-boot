package com.CIconGitLabySpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiConGitLabYSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(CiConGitLabYSpringBootApplication.class, args);
	}

}
